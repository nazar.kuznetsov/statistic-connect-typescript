FROM gradle:4.10-jdk11 as builder

COPY --chown=gradle:gradle . /home/gradle/project

WORKDIR /home/gradle/project

RUN gradle clean build -x test

FROM openjdk:11-jre

COPY --from=builder /home/gradle/project/build/libs/project-0.0.1.jar app.jar

VOLUME /tmp

EXPOSE 8080

CMD java -Djava.security.egd=file:/dev/./urandom -jar app.jar
