CREATE TABLE feedbacks(
  comment VARCHAR (200),
  author_id BIGINT,
  recipient_id BIGINT,
  created_at TIMESTAMP,
  rating FLOAT,
  period_id BIGINT,
  type VARCHAR (20),
  status VARCHAR (20)
);

ALTER TABLE feedbacks ADD COLUMN id SERIAL PRIMARY KEY NOT NULL;