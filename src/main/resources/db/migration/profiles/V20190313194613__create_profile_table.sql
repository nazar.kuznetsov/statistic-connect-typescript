create table profiles
(
  id bigint not null
    constraint profiles_pk
      primary key,
  email VARCHAR(255),
  profile_name VARCHAR(255) not null,
  avatarURL VARCHAR(500)
);
