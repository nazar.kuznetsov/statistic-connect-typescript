package com.spdu.feedbacksystem.user;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    USER, TL, PM, ADMIN, MANAGER;

    @Override
    public String getAuthority() {
        return "ROLE_" + name();
    }
}
