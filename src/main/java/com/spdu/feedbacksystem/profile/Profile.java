package com.spdu.feedbacksystem.profile;

import java.util.Objects;

public class Profile {
    private long id;
    private String email;
    private String profileName;
    private String avatarURL;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return id == profile.id &&
                Objects.equals(email, profile.email) &&
                Objects.equals(profileName, profile.profileName) &&
                Objects.equals(avatarURL, profile.avatarURL);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, profileName, avatarURL);
    }
}
