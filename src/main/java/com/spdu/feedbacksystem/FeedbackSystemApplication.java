package com.spdu.feedbacksystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedbackSystemApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(FeedbackSystemApplication.class);
        springApplication.run(args);
    }
}
