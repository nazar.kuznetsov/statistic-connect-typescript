package com.spdu.feedbacksystem.notifications;

public interface MailService {
    public void send(String emailTo, String subject, String message);
}
