package com.spdu.feedbacksystem.notifications;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;



@Service
public class MailServiceImpl implements MailService {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MailContentBuilder mailContentBuilder;

    private static final Logger logger = Logger.getLogger(MailServiceImpl.class.getName());

    @Value("${spring.mail.username}")
    private String username;


    public void send(String emailTo, String subject, String message) {

        String content = mailContentBuilder.build(message);

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(username);
            messageHelper.setTo(emailTo);
            messageHelper.setSubject(subject);
            messageHelper.setText(content, true);
        };

        try{
            mailSender.send(messagePreparator);
        }catch(MailException e){
            String errorMassage = String.format("Error happened while sending email to %s", emailTo);
            logger.error(errorMassage);
        }



    }
}
