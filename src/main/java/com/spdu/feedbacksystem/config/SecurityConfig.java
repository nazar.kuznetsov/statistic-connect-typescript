package com.spdu.feedbacksystem.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

    @Autowired
    public SecurityConfig() {
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/auth").permitAll()
                .antMatchers("/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .and()
                .formLogin()
                .loginProcessingUrl("/auth")
                .failureUrl("/login?fall=true")
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .permitAll()
                .and()
                .rememberMe();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
                auth.inMemoryAuthentication()
                .withUser("TL")
                .password("{noop}password")
                .roles("TL", "USER")
                .and()

                .withUser("PM")
                .password("{noop}password")
                .roles("PM", "USER")
                .and()

                .withUser("Admin")
                .password("{noop}password")
                .roles("ADMIN", "USER")
                .and()

                .withUser("User")
                .password("{noop}password")
                .roles("USER")
                .and()

                .withUser("Manager")
                .password("{noop}password")
                .roles("MANAGER", "USER")
                .and()

                .withUser("TLPM")
                .password("{noop}password")
                .roles("TL", "PM", "USER")
                .and()

                .withUser("ManagerPM")
                .password("{noop}password")
                .roles("MANAGER", "PM", "USER")
                .and()

                .withUser("ManagerAdmin")
                .password("{noop}password")
                .roles("MANAGER", "ADMIN", "USER");
    }

}
