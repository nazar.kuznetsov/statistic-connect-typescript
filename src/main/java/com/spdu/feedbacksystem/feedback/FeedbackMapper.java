package com.spdu.feedbacksystem.feedback;

import com.spdu.feedbacksystem.feedback.models.Feedback;
import com.spdu.feedbacksystem.feedback.models.constants.FeedbackStatus;
import com.spdu.feedbacksystem.feedback.models.constants.FeedbackType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeedbackMapper implements RowMapper<Feedback> {

    @Override
    public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
        var feedback = new Feedback();

        feedback.setAuthorId(rs.getLong("author_id"));
        feedback.setRecipientId(rs.getLong("recipient_id"));
        feedback.setPeriodId(rs.getLong("period_id"));
        feedback.setComment(rs.getString("comment"));
        feedback.setFeedbackStatus(FeedbackStatus
                .valueOf(rs.getString("status")));
        feedback.setFeedbackType(FeedbackType
                .valueOf(rs.getString("type")));
        feedback.setRating(rs.getFloat("rating"));
        feedback.setCreatedAt(rs.getTimestamp("created_at")
                .toLocalDateTime());
        feedback.setId(rs.getLong("id"));

        return feedback;
    }
}
