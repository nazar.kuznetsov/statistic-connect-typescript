package com.spdu.feedbacksystem.feedback;

import com.spdu.feedbacksystem.feedback.converters.FeedbackConverter;
import com.spdu.feedbacksystem.feedback.models.FeedbackDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/feedback")
public class FeedbackController {
    private final FeedbackService feedbackService;
    private final FeedbackConverter feedbackConverter;

    @Autowired
    public FeedbackController(FeedbackService feedbackService, FeedbackConverter feedbackConverter) {
        this.feedbackService = feedbackService;
        this.feedbackConverter = feedbackConverter;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody FeedbackDto feedbackDto) {
        feedbackService.save(feedbackConverter.dtoToFeedback(feedbackDto));
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("{authorId}")
    public ResponseEntity getAllOwn(@PathVariable long authorId) {
        var feedbacks = feedbackService.getAllFeedbacksByAuthorID(authorId);
        return new ResponseEntity(feedbacks, HttpStatus.OK);
    }
}
