package com.spdu.feedbacksystem.feedback.converters;

import com.spdu.feedbacksystem.feedback.models.Feedback;
import com.spdu.feedbacksystem.feedback.models.FeedbackDto;
import org.springframework.stereotype.Component;

@Component
public class FeedbackConverterImpl implements FeedbackConverter {

    public FeedbackDto feedbackToDto(Feedback feedback) {
        var feedbackDto = new FeedbackDto();

        feedbackDto.setAuthorId(feedback.getAuthorId());
        feedbackDto.setComment(feedback.getComment());
        feedbackDto.setFeedbackType(feedback.getFeedbackType());
        feedbackDto.setPeriodId(feedback.getPeriodId());
        feedbackDto.setRating(feedback.getRating());
        feedbackDto.setRecipientId(feedback.getRecipientId());

        return feedbackDto;
    }

    public Feedback dtoToFeedback(FeedbackDto feedbackDto) {
        var feedback = new Feedback();

        feedback.setAuthorId(feedbackDto.getAuthorId());
        feedback.setComment(feedbackDto.getComment());
        feedback.setFeedbackType(feedbackDto.getFeedbackType());
        feedback.setPeriodId(feedbackDto.getPeriodId());
        feedback.setRating(feedbackDto.getRating());
        feedback.setRecipientId(feedbackDto.getRecipientId());

        return feedback;
    }
}
