package com.spdu.feedbacksystem.feedback.converters;

import com.spdu.feedbacksystem.feedback.models.Feedback;
import com.spdu.feedbacksystem.feedback.models.FeedbackDto;

public interface FeedbackConverter {
    FeedbackDto feedbackToDto(Feedback feedback);

    Feedback dtoToFeedback(FeedbackDto feedbackDto);

}
