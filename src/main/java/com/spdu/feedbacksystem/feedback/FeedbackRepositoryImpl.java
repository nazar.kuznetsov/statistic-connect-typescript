package com.spdu.feedbacksystem.feedback;

import com.spdu.feedbacksystem.feedback.models.Feedback;
import com.spdu.feedbacksystem.feedback.models.constants.FeedbackStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public FeedbackRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Feedback getById(long id) {
        MapSqlParameterSource feedbackFields = new MapSqlParameterSource();
        feedbackFields.addValue("id", id);

        String query = "SELECT * FROM feedbacks WHERE id = :id";

        Feedback feedback = jdbcTemplate.queryForObject(query, feedbackFields, new FeedbackMapper());
        return feedback;
    }

    @Override
    public List<Feedback> getAllFeedbacksByAuthorID(long authorId) {
        MapSqlParameterSource feedbackFields = new MapSqlParameterSource();
        feedbackFields.addValue("author_id", authorId);

        String query = "SELECT * FROM feedbacks WHERE author_id =  :author_id";

        List<Feedback> feedbacks = jdbcTemplate.query(query, feedbackFields,
                rs -> {
                    List<Feedback> list = new ArrayList<>();
                    while (rs.next()) {
                        list.add(new FeedbackMapper().mapRow(rs, rs.getRow()));
                    }
                    return list;
                });
        return feedbacks;
    }

    @Override
    public void save(Feedback feedback) {
        String query = "INSERT INTO feedbacks ( " +
                "comment, author_id, " +
                "recipient_id, created_at, " +
                "rating, period_id, " +
                "type, status ) " +
                "VALUES (:comment, :author_id, " +
                ":recipient_id, :created_at, " +
                ":rating, :period_id, " +
                ":type, :status)";

        MapSqlParameterSource feedbackFields = setFeedbackData(feedback);
        jdbcTemplate.update(query, feedbackFields);
    }

    private MapSqlParameterSource setFeedbackData(Feedback feedback) {
        MapSqlParameterSource feedbackFields = new MapSqlParameterSource();

        feedbackFields.addValue("comment", feedback.getComment());
        feedbackFields.addValue("author_id", feedback.getAuthorId());
        feedbackFields.addValue("recipient_id", feedback.getRecipientId());
        feedbackFields.addValue("created_at", LocalDateTime.now());
        feedbackFields.addValue("rating", feedback.getRating());
        feedbackFields.addValue("period_id", feedback.getPeriodId());
        feedbackFields.addValue("type", feedback.getFeedbackType().name());
        feedbackFields.addValue("status", FeedbackStatus.NEW.name());

        return feedbackFields;
    }
}
