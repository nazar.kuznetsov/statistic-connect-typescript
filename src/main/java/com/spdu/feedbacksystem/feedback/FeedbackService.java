package com.spdu.feedbacksystem.feedback;

import com.spdu.feedbacksystem.feedback.models.Feedback;

import java.util.List;

public interface FeedbackService {
    void save(Feedback feedback);

    List<Feedback> getAllFeedbacksByAuthorID(long authorId);
}
