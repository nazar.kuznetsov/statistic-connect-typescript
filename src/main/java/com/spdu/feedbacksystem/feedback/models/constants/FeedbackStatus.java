package com.spdu.feedbacksystem.feedback.models.constants;

public enum FeedbackStatus {
    NEW,
    SUBMITTED,
    REJECTED
}
