package com.spdu.feedbacksystem.feedback.models;

import com.spdu.feedbacksystem.feedback.models.constants.FeedbackStatus;
import com.spdu.feedbacksystem.feedback.models.constants.FeedbackType;

import java.time.LocalDateTime;
import java.util.Objects;

public class Feedback {
    private long id;
    private String comment;
    private long authorId;
    private long recipientId;
    private LocalDateTime createdAt;
    private long periodId;
    private FeedbackType feedbackType;
    private FeedbackStatus feedbackStatus;
    private float rating;

    public Feedback() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public long getPeriodId() {
        return periodId;
    }

    public void setPeriodId(long periodId) {
        this.periodId = periodId;
    }

    public FeedbackType getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(FeedbackType feedbackType) {
        this.feedbackType = feedbackType;
    }

    public FeedbackStatus getFeedbackStatus() {
        return feedbackStatus;
    }

    public void setFeedbackStatus(FeedbackStatus feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Feedback feedback = (Feedback) o;
        return id == feedback.id &&
                authorId == feedback.authorId &&
                recipientId == feedback.recipientId &&
                periodId == feedback.periodId &&
                Float.compare(feedback.rating, rating) == 0 &&
                Objects.equals(comment, feedback.comment) &&
                Objects.equals(createdAt, feedback.createdAt) &&
                feedbackType == feedback.feedbackType &&
                feedbackStatus == feedback.feedbackStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, comment, authorId, recipientId, createdAt, periodId, feedbackType, feedbackStatus, rating);
    }
}
