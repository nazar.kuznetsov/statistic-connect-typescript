package com.spdu.feedbacksystem.feedback.models.constants;

public enum FeedbackType {
    OPTIONAL,
    QUALITY
}
