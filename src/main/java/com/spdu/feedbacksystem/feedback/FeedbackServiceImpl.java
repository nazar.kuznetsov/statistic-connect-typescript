package com.spdu.feedbacksystem.feedback;

import com.spdu.feedbacksystem.feedback.models.Feedback;
import com.spdu.feedbacksystem.feedback.models.FeedbackDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Autowired
    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public void save(Feedback feedback) {
        feedbackRepository.save(feedback);
    }

    @Override
    public List<Feedback> getAllFeedbacksByAuthorID(long authorId) {
        return feedbackRepository.getAllFeedbacksByAuthorID(authorId);
    }
}
