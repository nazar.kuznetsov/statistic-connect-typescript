package com.spdu.feedbacksystem.feedback;

import com.spdu.feedbacksystem.feedback.models.Feedback;

import java.util.List;

public interface FeedbackRepository {
    void save(Feedback feedback);

    Feedback getById(long id);

    List<Feedback> getAllFeedbacksByAuthorID(long authorId);
}
