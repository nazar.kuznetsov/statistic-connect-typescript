import {IFeedback} from './services/type';

export interface IMapProps {
  statisticReducer: {
    data: IFeedback[]
  };
}

export interface IMyActionType {
  type: string;
  payload: {
    id: string
  };
}

export interface IProps {
  dataTable: IFeedback[];
  statisticDelete: (id: string) => void;
  sendNotification: (id: string) => void;
}
