import React, {Component, Dispatch} from 'react';
import Table from './table';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {IFeedback} from './services/type';
import {statisticDelete, sendNotification} from './services/action';
import {IMapProps, IMyActionType, IProps} from './type';

class Statistic extends Component<IProps> {
    state = {
        valueUser: '',
        valueAuthor: '',
        derection: ''
    };

    componentDidMount() {
        // fetch request initial state redux
    }

    /* Значение для поиска valueUser
    ====================================================================== */
    setFilterUser = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({valueUser: e.target.value.toLowerCase()});
    }

    /* Значение для поиска valueAuthor
    ====================================================================== */
    setFilterAuthor = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({valueAuthor: e.target.value.toLowerCase()});
    }

    /* Удалить элемент из таблицы
    ====================================================================== */
    delete = (id: string) => {
        this.props.statisticDelete(id);
    }

    /* Отправить уведомление
    ====================================================================== */
    sendNotification = (id: string) => {
        this.props.sendNotification(id);
    }

    /* Установить направление сортировки по рейтингу
    ====================================================================== */
    sortingRating = (type: string) => {
        this.setState({derection: type});
    }

    /* Сортировать по рейтингу
    ====================================================================== */
    derection = (data: IFeedback[]) => {
        switch (this.state.derection) {
            case 'desc':
                return [...data].sort((a, b) => a.rating - b.rating);
            case 'asc':
                return [...data].sort((a, b) => b.rating - a.rating);
            default: return data;
        }
    }

    render() {
        let data = this.props.dataTable
            .filter(element => element.name.toLowerCase().includes(this.state.valueUser))
            .filter(element => element.author.toLowerCase().includes(this.state.valueAuthor));

        data = this.derection(data);

        return (
            <>
                <Table
                    data={data}
                    sortingRating={this.sortingRating}
                    delete={this.delete}
                    sendNotification={this.sendNotification}
                    filterUser={this.setFilterUser}
                    filterAuthor={this.setFilterAuthor}
                />
            </>
        );
    }
}

const mapStateToProps = ({statisticReducer}: IMapProps) => {
    return {
        dataTable: statisticReducer.data
    };
};

const mapDispatchToProps = (dispatch: Dispatch<IMyActionType>) => {
    return {
        statisticDelete: (id: string) => dispatch(statisticDelete(id)),
        sendNotification: (id: string) => dispatch(sendNotification(id))
    };
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps)
)(Statistic);
