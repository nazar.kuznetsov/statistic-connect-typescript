export interface IFeedback {
    author: string;
    name: string;
    rating: number;
    comments: string;
    id: string;
}

export interface IAction {
    type: string;
    payload: {
        id: string
    };
}
