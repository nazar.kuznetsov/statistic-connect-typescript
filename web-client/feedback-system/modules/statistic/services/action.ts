import {STATISTIC_DELETE, STATISTIC_SEND_NOTIFICATION} from './constans';

export const statisticDelete = (id: string) => {
    return {
        type: STATISTIC_DELETE,
        payload: {id}
    };
};

export const sendNotification = (id: string) => {
    return {
        type: STATISTIC_SEND_NOTIFICATION,
        payload: {id}
    };
};
