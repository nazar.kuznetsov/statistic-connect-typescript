import {STATISTIC_DELETE} from './constans';
import {IFeedback, IAction} from './type';

const api: IFeedback[] = [
    {
        author: 'Андрей Кличко',
        id: '001',
        name: 'Тлекс Дрон',
        rating: 3,
        comments: 'Ок'
    },
    {
        author: 'Петро Порошенко',
        id: '002',
        rating: 2,
        name: 'Макс Опг',
        comments: 'Молодец'
    },
    {
        author: 'Виктор Янукович',
        id: '003',
        name: 'Алкоголик Черный',
        rating: 1,
        comments: 'Плохо'
    },
    {
        author: 'Аладимир Путин',
        id: '004',
        name: 'Янукович Виктор',
        rating: 4,
        comments: 'Молодец'
    },
    {
        author: 'Барак Обама',
        id: '005',
        rating: 8,
        name: 'Космос Олеп',
        comments: 'Класс'
    },
    {
        author: 'Ким Чен Ир',
        id: '006',
        rating: 7,
        name: 'Владимир Кличко',
        comments: 'Круто'
    }
];

const initialState = {
    data: api
};

export default (state = initialState, action: IAction) => {
    switch (action.type) {
        case STATISTIC_DELETE:
            return {
                ...state, data: state.data.filter(element => element.id !== action.payload.id)
            };
        default: return state;
    }
};
