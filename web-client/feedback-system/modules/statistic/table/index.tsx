import React, {Component, ChangeEvent} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Feedback from './feedback';
import Title from './title';
import {TableCell, TextField} from '@material-ui/core';
import {IFeedback} from '../services/type';
import {styles} from './styles';

interface IProps {
  classes: {
    root: string;
    table: string;
  };
  sortingRating: (direction: string) => void;
  delete: (id: string) => void;
  sendNotification: (id: string) => void;
  filterUser: (event: ChangeEvent<HTMLInputElement>) => void;
  filterAuthor: (event: ChangeEvent<HTMLInputElement>) => void;
  data: IFeedback[];
}

interface IState {
  direction: 'desc' | 'asc';
}

class SimpleTable extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            direction: 'desc'
        };
    }
  sortingRating = () => {
      this.setState(({direction}) => {
          return {direction: direction === 'desc' ? 'asc' : 'desc'};
      }, () => this.props.sortingRating(this.state.direction));

  }

  render() {
      const {classes} = this.props;
      return (
          <Paper className={classes.root}>
              <Table className={classes.table}>
                  <TableHead>
                      <TableRow>
                          <TableCell>
                              <TextField
                                  id="standard-dense"
                                  label="Автор"
                                  onChange={this.props.filterAuthor}
                              />
                          </TableCell>
                          <TableCell>
                              <TextField
                                  id="standard-dense2"
                                  label="Пользователь"
                                  onChange={this.props.filterUser}
                              />
                          </TableCell>
                          <Title
                              orderBy={this.state.direction}
                              handleClick={this.sortingRating}
                          />
                          <TableCell>Комментарий</TableCell>
                      </TableRow>
                  </TableHead>
                  <TableBody>
                      {
                          this.props.data.length > 0
                              ? this.props.data.map(row => (
                                  <TableRow key={row.id}>
                                      <Feedback
                                          author={row.author}
                                          name={row.name}
                                          rating={row.rating}
                                          sendNotification={this.props.sendNotification}
                                          id={row.id}
                                          comments={row.comments}
                                          onDelete={this.props.delete}
                                      />
                                  </TableRow>
                              ))
                              : <TableRow>
                                  <TableCell>Не найдено</TableCell>
                              </TableRow>
                      }
                  </TableBody>
              </Table>
          </Paper>
      );
  }
}

export default withStyles(styles)(SimpleTable);
