import React from 'react';
import {TableCell, TableSortLabel} from '@material-ui/core';

interface ITitle {
    orderBy: 'asc' | 'desc';
    handleClick: () => void;
}

const Title = ({handleClick, orderBy}: ITitle) => {
    const click = () => {
        handleClick();
    };

    return (
        <TableCell
            onClick={click}>
            <TableSortLabel
                active={true}
                direction={orderBy}
            />
        </TableCell>
    );
};

export default Title;
