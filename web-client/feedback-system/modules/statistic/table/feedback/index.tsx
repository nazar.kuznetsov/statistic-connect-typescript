import React from 'react';
import {TableCell} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import RestoreIcon from '@material-ui/icons/Restore';

interface IFeedback {
  name: string;
  author: string;
  rating: number;
  comments: string;
  id: string;
  onDelete: (id: string) => void;
  sendNotification: (id: string) => void;
}

const Feedback = ({name, author, id, rating, comments, onDelete, sendNotification}: IFeedback) => {

    const handleClick = () => {
        onDelete(id);
    };

    const notification = () => {
        sendNotification(id);
    };

    return (
    <>
      <TableCell>{author}</TableCell>
      <TableCell>{name}</TableCell>
      <TableCell>{rating}</TableCell>
      <TableCell>{comments}</TableCell>

      <TableCell onClick={notification} padding="checkbox">
          <RestoreIcon />
      </TableCell>

      <TableCell
          onClick={handleClick}
          padding="checkbox">
          <DeleteIcon />
      </TableCell>
    </>
    );
};

export default Feedback;
