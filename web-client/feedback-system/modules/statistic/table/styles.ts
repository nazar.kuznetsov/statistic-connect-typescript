import {createStyles, WithStyles} from '@material-ui/core';
import {Theme} from 'feedback-system/with-root/typedef';

export const styles = (theme: Theme) => createStyles({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto'
    },
    table: {
        minWidth: 700
    }
});

export interface IClasses extends WithStyles<typeof styles> { }
