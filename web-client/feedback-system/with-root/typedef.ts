import theme from './global-theme';

export type Theme = typeof theme;
