import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import green from '@material-ui/core/colors/green';
import blue from '@material-ui/core/colors/blue';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
        fontFamily: [
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"'
        ].join(',')
    },
    palette: {
        primary: {
            light: blue[700],
            main: blue[800],
            dark: blue[900]
        },
        secondary: {
            light: green[300],
            main: green[600],
            dark: green[700]
        }
    }
});

export default theme;
