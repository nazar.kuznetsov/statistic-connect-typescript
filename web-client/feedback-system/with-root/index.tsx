import React, {ComponentType} from 'react';
import {MuiThemeProvider} from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline/CssBaseline';
import theme from './global-theme';

const withRoot = (Component: ComponentType<any>) => {
    const WithRoot = (props: any) => {
        // MuiThemeProvider makes the theme available down the React tree
        // thanks to React context.
        return (
            <MuiThemeProvider theme={theme}>
                {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                <CssBaseline />
                <Component {...props} />
            </MuiThemeProvider>
        );
    };

    return WithRoot;
};

export default withRoot;
