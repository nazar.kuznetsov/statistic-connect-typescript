import {Theme} from './typedef';

const globalStyles = (theme: Theme) => ({
    '@global': {
        '#root': {
            width: '100%',
            height: '100%'
        }
    }
});

export default globalStyles;
