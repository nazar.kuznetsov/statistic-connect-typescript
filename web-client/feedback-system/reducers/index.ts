import {combineReducers} from 'redux';
import {mockReducer} from '../modules/albums/controllers/services/reducers';
import {statisticReducer} from '../modules/statistic';

const rootReducer = combineReducers({
    mockReducer,
    statisticReducer
});

export default rootReducer;
